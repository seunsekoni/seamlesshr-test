<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'api'], function ($router) {

    // register new user
    Route::post('register', 'AuthController@register');

    // Login registered user
    Route::post('login', 'AuthController@login')->name('login');
    // Logout authenticated user
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

    // Courses 
    Route::get('create/courses', ['uses' => 'CourseController@create', 'as' => 'course.create']);
    Route::get('download/courses', ['uses' => 'CourseController@download_courses', 'as' => 'courses.download']);

    // Users
    // User cn register a course
    Route::post('add/course', ['uses' => 'UserController@add_course', 'as' => 'add.course']);
    Route::get('get/courses', ['uses' => 'UserController@get_courses', 'as' => 'get.courses']);
});
