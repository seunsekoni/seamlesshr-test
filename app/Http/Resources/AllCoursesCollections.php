<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AllCoursesCollections extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text' => $this->text,
            'unit' => $this->unit,
            'department' => $this->department,
            'description' => $this->description,
            'registered_users' => 
                $this->users

        ];

        

    }
}
