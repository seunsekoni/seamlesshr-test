<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Jobs\CreateCourseJob;
use Carbon\Carbon;

use App\Exports\CoursesExport;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    /**
     * Create new courses
     */
    public function create()
    {
        try {
            // fire the job queue that will be used in creating new courses
            $courses = CreateCourseJob::dispatch();
            if($courses) return $this->sendResponse($courses,'Courses created successfully');
        } catch (\Throwable $th) {
            return $this->sendServerError('Failed to create Courses');
        }
    }

    /**
     * Export and download all courses
     */
    public function download_courses()
    {
        return Excel::download(new CoursesExport, 'courses.csv');
    }
}
