<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }

    /**
     * Handles server error response
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendServerError($message)
    {
        return response()->json([
            'success'   => false,
            'message'   => $message
        ], 500);
    }

    /**
     * Return validation error response.
     *
     * @return \Illuminate\Http\Response
     */
    public static function validationErrorResponse($data)
    {
        return response()->json([
            'success'   => false,
            'data'      => $data,
            'message'   => 'Failed validation.'
        ]);
    }
}
