<?php

namespace App\Http\Controllers;
use App\Course;

use Illuminate\Http\Request;
use App\Http\Resources\AllCoursesCollections;


class UserController extends Controller
{
    /**
     * Register a student for a course
     */
    public function add_course(Request $request)
    {
        // check if required input will be passed
        $validator = Validator::make($request->all(), [
            'course_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->validationErrorResponse($validator->errors());
        }
        try {
            // get the authenticated user id
            $user = auth()->user()->id;
            
            // get the course id from the request
            $course_id = $request->get('course_id');
            $course = Course::find($course_id);
            
            // Check if the user has previously been registered to a course
            if(!$course->users->contains($user)) {
                // attach both the course and users and store in the db
                $course->users()->attach($user);
                return $this->sendResponse($course,'Course registered to user successfully');
            } else {
                return $this->sendServerError('User has previously registered this course');
            }
        } catch (\Throwable $th) {
            return $this->sendServerError('Failed to register course');
        }
    }

    /**
     * Get list of course
     */
    public function get_courses()
    {
        try {
            // Fetch all courses
            $courses = Course::all();
            // eager load all users registered courses
            $allCourses = $courses->load(['users' => function($query) {
                $userId = auth()->user()->id;
                $query->where('course_user.user_id', $userId);
            }]);
    
            return $this->sendResponse(AllCoursesCollections::collection($allCourses), 'Courses retrieved successfully');
        } catch (\Throwable $th) {
            return $this->sendServerError('Failed to retrieve courses');
        }
        
    }
}
