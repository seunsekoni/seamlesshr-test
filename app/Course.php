<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    // create many to many relationship with the user model
    public function users()
    {
        return $this->belongsToMany('App\User', 'course_user')->withTimestamps();
    }
}
