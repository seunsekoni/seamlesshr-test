<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->realText($maxNbChars = 20, $indexSize = 2),
        'unit' => $faker->numberBetween($min = 0, $max = 5),
        'department' => $faker->company,
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
    ];
});
